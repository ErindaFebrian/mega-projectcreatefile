package com.example.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BacaFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(BacaFileApplication.class, args);
	}

}
